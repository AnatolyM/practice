#!/bin/bash
set -e
if [ ! -z "$MY_NAME" ]
then
  echo "MY_NAME = '${MY_NAME}'" >> /usr/share/nginx/html/config.js;
fi

if [ ! -z "$MY_SECRET_NAME" ]
then
  echo "MY_SECRET_NAME = '${MY_SECRET_NAME}'" >> /usr/share/nginx/html/config.js;
fi

exec "$@"
